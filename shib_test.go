package shibreg

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	_, err := New()
	require.EqualError(t, err, "must set username")

	_, err = New(WithUsername("username"))
	require.EqualError(t, err, "must set password")

	_, err = New(WithUsername("username"), WithPassword("<PASSWORD>"))
	require.NoError(t, err)
}

func TestUnixTimeUnmarshal(t *testing.T) {
	given := 1708373996000
	// given := 1712687387
	var got UnixTime
	require.NoError(t, json.Unmarshal([]byte(fmt.Sprint(given)), &got))
	require.Equal(t,
		UnixTime{Time: time.Date(2024, time.February, 19, 15, 19, 56, 0, time.Local)},
		got)
}

func TestList(t *testing.T) {
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		b, err := os.ReadFile("testdata/listing.json")
		panicIfErr(err)
		fmt.Fprint(w, string(b))
	}))
	defer svr.Close()
	client := mustNew(
		WithUsername("username"),
		WithPassword("password"),
		WithBaseURL(svr.URL),
	)
	got, err := client.List()
	require.NoError(t, err)
	require.Equal(t,
		&ListResponse{
			Data: []ListResponseData{
				{
					EntityID:          "https://testing-a.example.com",
					FunctionalPurpose: "Testing Site A",
					ID:                100,
					LastUpdated:       UnixTime{Time: time.Date(2024, time.February, 19, 15, 19, 56, 0, time.Local)},
					PublicName:        "Testing Site A Website",
				},
				{
					EntityID:          "https://testing-b.example.com",
					FunctionalPurpose: "Testing Site B",
					ID:                200,
					LastUpdated:       UnixTime{Time: time.Date(2014, time.January, 16, 14, 29, 18, 0, time.Local)},
					PublicName:        "Testing Site B Website",
				},
			},
		},
		got,
	)
}
