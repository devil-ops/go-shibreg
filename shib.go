/*
Package shibreg handles interacting with the shibboleth registration API
*/
package shibreg

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"strings"
	"time"

	"moul.io/http2curl"
)

// Client is the thing that interacts with the API
type Client struct {
	client             *http.Client
	username, password string
	debugCurl          bool
	baseURL            string
}

// New returns a new client and an optional error
func New(opts ...func(*Client)) (*Client, error) {
	c := &Client{
		client:  http.DefaultClient,
		baseURL: "https://idms-web-ws.oit.duke.edu",
	}
	for _, opt := range opts {
		opt(c)
	}
	if c.username == "" {
		return nil, errors.New("must set username")
	}
	if c.password == "" {
		return nil, errors.New("must set password")
	}
	return c, nil
}

// WithBaseURL sets a custom base url for a new client
func WithBaseURL(s string) func(*Client) {
	return func(c *Client) {
		c.baseURL = strings.TrimSuffix(s, "/") + "/" // Exactly 1 / at the end please
	}
}

// WithDebugCurl enables curl command debugging
func WithDebugCurl() func(*Client) {
	return func(c *Client) {
		c.debugCurl = true
	}
}

// WithUsername sets the username for a new client
func WithUsername(s string) func(*Client) {
	return func(c *Client) {
		c.username = s
	}
}

// WithPassword sets the password for a new client
func WithPassword(s string) func(*Client) {
	return func(c *Client) {
		c.password = s
	}
}

func mustNew(opts ...func(*Client)) *Client {
	got, err := New(opts...)
	if err != nil {
		panic(err)
	}
	return got
}

func (c *Client) sendRequest(req *http.Request, v interface{}) (*Response, error) {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")
	req.SetBasicAuth(c.username, c.password)

	if os.Getenv("PRINT_CURL") != "" {
		command, _ := http2curl.GetCurlCommand(req)
		slog.Info("curl equivalent", "command", command.String())
	}

	res, err := c.client.Do(req)
	req.Close = true
	if err != nil {
		slog.Warn("error submitting request", "error", err)
		return nil, err
	}

	defer dclose(res.Body)

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes ErrorResponse
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return nil, errors.New(errRes.Message)
		}

		return nil, fmt.Errorf("error, status code: %d", res.StatusCode)
	}

	b, _ := io.ReadAll(res.Body)
	if string(b) != "" {
		if err = json.NewDecoder(bytes.NewReader(b)).Decode(&v); err != nil {
			return nil, err
		}
	} else {
		// When there is no body
		return nil, nil
	}
	r := &Response{Response: res}

	return r, nil
}

// Response is a success response
type Response struct {
	*http.Response
}

// ErrorResponse represents an error response
type ErrorResponse struct {
	Message string `json:"errorMessage"`
	Error   bool   `json:"error"`
}

func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		slog.Error("error closing item", "error", err)
	}
}

// ListResponse is what we get back from the API when doing:
// GET /idm-ws/shibbolethRegistration
type ListResponse struct {
	Data  []ListResponseData `json:"data,omitempty"`
	Error bool               `json:"error,omitempty"`
}

// ListResponseData is the data portion of a ListResponse
type ListResponseData struct {
	EntityID          string   `json:"entityId,omitempty"`
	FunctionalPurpose string   `json:"functionalPurpose,omitempty"`
	ID                int64    `json:"id,omitempty"`
	LastUpdated       UnixTime `json:"lastUpdated,omitempty"`
	PublicName        string   `json:"publicName,omitempty"`
}

// UnixTime is our magic time type to use for marshaling/unmarshaling responses
type UnixTime struct {
	time.Time
}

// UnmarshalJSON is the method that satisfies the Unmarshaller interface
func (u *UnixTime) UnmarshalJSON(b []byte) error {
	var timestamp int64
	err := json.Unmarshal(b, &timestamp)
	if err != nil {
		return err
	}
	u.Time = time.UnixMilli(timestamp)
	return nil
}

// List returns a listing of registrations for the authenticated user
func (c *Client) List() (*ListResponse, error) {
	ret := &ListResponse{}
	req, err := http.NewRequest("GET", c.baseURL+"idm-ws/shibbolethRegistration", nil)
	if err != nil {
		return nil, err
	}
	if _, err = c.sendRequest(req, ret); err != nil {
		return nil, err
	}
	return ret, nil
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

// GetResponse is the raw response for a get request for use by the endpoints:
// GET idm-ws/shibbolethRegistration/{id}
// GET idm-ws/shibbolethRegistration/findByEntityId?entityId={entityId}
type GetResponse struct {
	Data  GetResponseData `json:"data,omitempty"`
	Error bool            `json:"error,omitempty"`
}

// GetResponseData is the juicy part of a get response
type GetResponseData struct {
	ID                int64       `json:"id,omitempty"`
	Audience          string      `json:"audience,omitempty"`
	Certificate       string      `json:"certificate,omitempty"`
	EncryptAssertions bool        `json:"encryptAssertions,omitempty"`
	EntityID          string      `json:"entityId,omitempty"`
	FunctionOwnerDept string      `json:"functionOwnerDept,omitempty"`
	FunctionalPurpose string      `json:"functionalPurpose,omitempty"`
	LastUpdated       UnixTime    `json:"lastUpdated,omitempty"`
	PublicName        string      `json:"publicName,omitempty"`
	ResponsibleDept   string      `json:"responsibleDept,omitempty"`
	SignAssertions    bool        `json:"signAssertions,omitempty"`
	SignResponses     bool        `json:"signResponses,omitempty"`
	Users             []string    `json:"users,omitempty"`
	Contacts          []Contact   `json:"contacts,omitempty"`
	ACSURLs           []ACSURL    `json:"acsUrls,omitempty"`
	Attributes        []Attribute `json:"attributes,omitempty"`
	// Need an example of these before putting them in
	// SupportGroup      interface{}   `json:"supportGroup,omitempty"`
	// Groups            []interface{} `json:"groups,omitempty"`
}

// ACSURL represents an ACS URL
type ACSURL struct {
	Binding   string `json:"binding,omitempty"`
	IsDefault bool   `json:"isDefault,omitempty"`
	URL       string `json:"url,omitempty"`
}

// Attribute is...an attribute for a registration
type Attribute struct {
	Name string `json:"name,omitempty"`
}

// Contact is a contact for a given registration
type Contact struct {
	Email string `json:"email,omitempty"`
	Netid string `json:"netid,omitempty"`
	Type  string `json:"type,omitempty"`
}
